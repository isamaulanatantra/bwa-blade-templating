<?php

use App\Http\Controllers\Landingcontroller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('clubs', [Landingcontroller::class, 'clubs'])->name('landing.clubs');
Route::get('players', [Landingcontroller::class, 'players'])->name('landing.players');
Route::get('managers', [Landingcontroller::class, 'managers'])->name('landing.managers');
Route::get('stadiums', [Landingcontroller::class, 'stadiums'])->name('landing.stadiums');
Route::resource('/', Landingcontroller::class);
